/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.view;

import br.com.senac.model.Contato;
import javax.swing.JOptionPane;
import agenda.Agenda;

/**
 *
 * @author sala302b
 */
public class JFrameCadastro extends javax.swing.JFrame {

    private JFramePrincipal jFramePrincipal;
    
    public JFrameCadastro() {
        initComponents();
        this.setVisible(true);
    }
    
    public JFrameCadastro(JFramePrincipal jFramePrincipal){
        this();
        this.jFramePrincipal = jFramePrincipal;
        this.setVisible(true);
    }
    
    private void salvar(){
        Contato contato = new Contato();
        
        contato.setNome(this.JTextFildNome.getText());
        contato.setTelefone(this.JTextFildTelefone.getText());
        contato.setEmail(this.JTextFildEmail.getText());
        contato.setTipoContato(this.JComboBoxTipoContato.getSelectedItem().toString());
        
        Agenda.listaDeContato.add(contato);
        
        this.jFramePrincipal.atualizarContador();
        
        JOptionPane.showMessageDialog(this, "Conato Salvo", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
        
        this.limpar();
    }
    
    private void limpar(){
        this.JTextFildNome.setText("");
        this.JTextFildTelefone.setText("");
        this.JTextFildEmail.setText("");
        this.JComboBoxTipoContato.setSelectedIndex(0);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        JTextFildTelefone = new javax.swing.JFormattedTextField();
        JComboBoxTipoContato = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        JTextFildNome = new javax.swing.JTextField();
        JTextFildEmail = new javax.swing.JTextField();
        jButtonSalvar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel1.setText("Nome:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 40, 30));

        jLabel2.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel2.setText("Telefone:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, -1, 30));

        jLabel3.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        jLabel3.setText("E-mail:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 160, -1, 30));

        jLabel4.setText("Tipo de Contato:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 210, 110, 30));

        try {
            JTextFildTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)#####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(JTextFildTelefone, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 120, 160, 30));

        JComboBoxTipoContato.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Residêncial", "Comercial", " " }));
        jPanel1.add(JComboBoxTipoContato, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 210, 160, 30));

        jLabel5.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        jLabel5.setText("Dados do Contato");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 20, -1, -1));
        jPanel1.add(JTextFildNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 80, 160, 30));
        jPanel1.add(JTextFildEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 160, 160, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 6, 480, 290));

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonSalvarMouseClicked(evt);
            }
        });
        getContentPane().add(jButtonSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 310, 80, 30));

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseClicked(evt);
            }
        });
        getContentPane().add(jButtonCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 310, 90, 30));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSalvarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarMouseClicked
        this.salvar();
    }//GEN-LAST:event_jButtonSalvarMouseClicked

    private void jButtonCancelarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseClicked
        this.limpar();
        this.dispose();
        
    }//GEN-LAST:event_jButtonCancelarMouseClicked

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> JComboBoxTipoContato;
    private javax.swing.JTextField JTextFildEmail;
    private javax.swing.JTextField JTextFildNome;
    private javax.swing.JFormattedTextField JTextFildTelefone;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
